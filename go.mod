module gitlab.com/diamondburned/lsg

require (
	github.com/acarl005/stripansi v0.0.0-20180116102854-5a71ef0e047d // indirect
	github.com/acarl005/textcol v0.0.0
	github.com/fatih/color v1.7.0
	github.com/fvbommel/util v0.0.0-20180919145318-efcd4e0f9787
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/urfave/cli v1.20.0
)
