package main

import (
	"os"
	"path/filepath"

	"github.com/urfave/cli"
)

func application(ctx *cli.Context) error {
	var path = ctx.Args().Get(0)
	if path == "" {
		path = "./"
	}

	sort := SortName

	switch SortStyle(sortMode) {
	case SortSize:
		sort = SortSize
	case SortTime:
		sort = SortTime
	}

	if !recursive {
		return Draw(path, sort)
	}

	return filepath.Walk(path, func(path string, i os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !i.IsDir() {
			return nil
		}

		return Draw(path, sort)
	})
}
