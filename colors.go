package main

import "github.com/fatih/color"

type fileColor struct {
	Name *color.Color

	// Optional for directory
	Ext *color.Color

	// Only used for Header
	Prefix string
}

// Some notes to take:
// - Color constants sit here: https://godoc.org/github.com/fatih/color#Attribute
// - "Hi" is just full bright, so HiWhite is white, and White is gray.

var colorMIMETypes = map[string]fileColor{
	// Deleting one of these keys will crash everything.
	"header": fileColor{
		Name: color.New(color.FgHiYellow),
		// Ext is used for the prefix of the header
		Ext:    color.New(color.FgYellow),
		Prefix: "===> ",
	},
	"directory": fileColor{
		Name: color.New(color.FgBlue, color.Bold),
	},
	"application": fileColor{
		Name: color.New(color.FgHiWhite),
		Ext:  color.New(color.FgHiWhite, color.Faint),
	},
	"text": fileColor{
		Name: color.New(color.FgHiCyan),
		Ext:  color.New(color.FgHiCyan, color.Faint),
	},
	"video": fileColor{
		Name: color.New(color.FgHiYellow),
		Ext:  color.New(color.FgHiYellow, color.Faint),
	},
	"audio": fileColor{
		Name: color.New(color.FgHiRed),
		Ext:  color.New(color.FgHiRed, color.Faint),
	},
	"image": fileColor{
		Name: color.New(color.FgHiMagenta),
		Ext:  color.New(color.FgHiMagenta, color.Faint),
	},
	"font": fileColor{
		Name: color.New(color.FgHiGreen),
		Ext:  color.New(color.FgHiGreen, color.Faint),
	},
}
