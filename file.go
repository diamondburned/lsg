package main

import (
	"bytes"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/diamondburned/lsg/sniff"
)

// GetNames returns the filenames into an array
func GetNames(path string, fs []os.FileInfo) []string {
	s := make([]string, 0, len(fs))
	for _, f := range fs {
		if !showHidden && strings.HasPrefix(f.Name(), ".") {
			continue
		}

		s = append(s, MakeName(path, f))
	}

	return s
}

// MakeName generates a name with colors
func MakeName(path string, i os.FileInfo) string {
	if i.IsDir() {
		return colorMIMETypes["directory"].Name.Sprint(i.Name() + "/")
	}

	if mime := readMIME(path, i); mime != "" {
		for m, c := range colorMIMETypes {
			if strings.HasPrefix(mime, m) {
				name, ext := splitNameExt(i.Name())
				if ext == "" {
					return c.Name.Sprint(name)
				}

				return c.Name.Sprint(name) + c.Ext.Sprint(ext)
			}
		}
	}

	return i.Name()
}

func readMIME(path string, i os.FileInfo) string {
	var buf bytes.Buffer

	f, err := os.Open(filepath.Join(path + i.Name()))
	if err != nil {
		return ""
	}

	defer f.Close()

	if _, err := io.CopyN(&buf, f, 512); err != nil && err != io.EOF {
		return ""
	}

	s := sniff.DetectContentType(buf.Bytes())
	if s == "" {
		return "text/plain"
	}

	return s
}
