package main

import (
	"os"

	"github.com/urfave/cli"
)

var (
	recursive  bool
	showHidden bool
	sortMode   string
	reverse    bool
)

func main() {
	app := cli.NewApp()
	app.Name = "lsg"

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "recursive, R",
			Usage:       "Recursively lists directories",
			Destination: &recursive,
		},
		cli.BoolFlag{
			Name:        "all, a",
			Usage:       "Show hidden files",
			Destination: &showHidden,
		},
		cli.StringFlag{
			Name:        "sort, s",
			Usage:       "Sort name, size or time",
			Value:       string(SortName),
			Destination: &sortMode,
		},
		cli.BoolFlag{
			Name:        "reverse, r",
			Usage:       "Reverse sort",
			Destination: &reverse,
		},
	}

	app.Action = application

	if err := app.Run(os.Args); err != nil {
		panic(err)
	}
}
