package main

import "path/filepath"

func splitNameExt(s string) (name string, ext string) {
	ext = filepath.Ext(s)
	return s[:len(s)-len(ext)], ext
}
