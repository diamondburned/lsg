package main

import (
	"os"
	"sort"

	"github.com/fvbommel/util/sortorder"
)

// SortStyle is the style to sort
type SortStyle string

const (
	// SortName sorts by name
	SortName SortStyle = "name"

	// SortSize sorts by size
	SortSize SortStyle = "size"

	// SortTime sorts by time
	SortTime SortStyle = "time"
)

// SortFiles sorts all the files
func SortFiles(fs []os.FileInfo, s SortStyle) {
	switch s {
	case SortSize:
		sort.Slice(fs, func(i, j int) bool {
			return fs[i].Size() < fs[j].Size()
		})
	case SortTime:
		sort.Slice(fs, func(i, j int) bool {
			return fs[i].ModTime().Before(fs[j].ModTime())
		})
	default:
		sort.Slice(fs, func(i, j int) bool {
			return sortorder.NaturalLess(fs[i].Name(), fs[j].Name())
		})
	}

	if reverse {
		sortReverse(fs)
	}

	sort.SliceStable(fs, func(i, j int) bool {
		return fs[i].IsDir()
	})
}

func sortReverse(s interface{}) {
	sort.SliceStable(s, func(i, j int) bool {
		return true
	})
}
