package main

import (
	"os"

	"github.com/acarl005/textcol"
)

// Draw draws the file
func Draw(path string, s SortStyle) (err error) {
	var files []os.FileInfo

	f, err := os.Open(path)
	if err != nil {
		return err
	}

	stat, err := f.Stat()
	if err != nil {
		return err
	}

	if stat.IsDir() {
		// Read the file as a directory
		files, err = f.Readdir(-1)
		if err != nil {
			return err
		}
	} else {
		files = []os.FileInfo{stat}
	}

	SortFiles(files, s)

	names := GetNames(path, files)

	c := colorMIMETypes["header"]
	c.Ext.Print(c.Prefix)
	c.Name.Println(path)

	textcol.PrintColumns(&names, 2)
	return nil
}
